package at.campus02.unogame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import at.campus02.database.unogame.DB_Manager_Uno;

public class UnoGame_Manager {

	private static final int MAXSCORE = 500;
	/**
	 * @author maroua.bousrih eva.loyens
	 * @version Uno Game 1.0
	 */

	private ArrayList<Card> deck = new ArrayList<>();// 108 cards
	private ArrayList<Card> cardPile = new ArrayList<>();// when a player throws card, all cards will be stocked in this
															// pile
	private ArrayList<Player> players = new ArrayList<>(); // list of player
	private DB_Manager_Uno managerDb = new DB_Manager_Uno();
	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	/**
	 * Constructor returns a deck (108 cards created from NumberCards and
	 * ActionCards) then the deck will be mixed
	 */
	public UnoGame_Manager() {
		// Deck creation
		CreateDeck();
	}

	/**
	 * <b>addPlayerToGame:</b> to add player to the game
	 * 
	 * @param maxplayer:
	 *            to identify how many player will be add to the game
	 */
	public void addPlayerToGame(int maxplayer) {
		int nbrPlayers = 0;
		try {

			String name;
			do {
				do {
					System.out.print("Give Player's name: ");
					name = br.readLine();
					if (!getListName().contains(name)) {
						break;
					}
					System.out.println("The name already exist give other name ");
				} while (getListName().contains(name));

				Player sp = new Player(name);
				addPlayer(sp);
				sp.setSession(1);
				sp.setRound(0);

				managerDb.addPlayer(sp.getName(), sp.getSession(), sp.getRound(), 0);
				nbrPlayers++;
			} while (nbrPlayers < maxplayer);
		} catch (IOException e) {
			e.printStackTrace();

		}

	}

	/**
	 * <b>addBotToGame:</b> to add bots to the game
	 * 
	 * @param maxplayer:
	 *            to identify how many bot will play
	 */
	public void addBotToGame(int maxplayer) {
		int nbrPlayers = 0;
		String nameBot;
		try {
			do {
				do {
					System.out.print("Give Bot's name: ");
					nameBot = br.readLine();

					if (!getListName().contains(nameBot)) {
						break;
					}
					System.out.println("The name already exist give other name ");
				} while (getListName().contains(nameBot));

				Player sp1 = new Bot(nameBot);
				// add the player to the game
				addPlayer(sp1);
				sp1.setSession(1);
				sp1.setRound(0);
				// registration of the player in DB
				managerDb.addPlayer(sp1.getName(), sp1.getSession(), sp1.getRound(), 0);
				nbrPlayers++;
			} while (nbrPlayers < maxplayer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * <b>CreateDeck: </b> to create a deck of 108 cards (created from NumberCards
	 * and ActionCards) then the deck will be mixed
	 */
	public void CreateDeck() {
		for (int index = 0; index < 1; index++) {
			for (int value = 0; value < 1; value++) {
				deck.add(new NumberCard('R', value));
				deck.add(new NumberCard('B', value));
				deck.add(new NumberCard('Y', value));
				deck.add(new NumberCard('G', value));
			}
		}

		// Number cards creation
		for (int index = 0; index < 2; index++) {
			for (int value = 1; value < 10; value++) {

				deck.add(new NumberCard('R', value));
				deck.add(new NumberCard('B', value));
				deck.add(new NumberCard('Y', value));
				deck.add(new NumberCard('G', value));
			}
		}

		for (int index = 0; index < 2; index++) {
			// Action cards creation
			// +2
			deck.add(new ActionCard('R', 20, "plus2"));
			deck.add(new ActionCard('B', 20, "plus2"));
			deck.add(new ActionCard('Y', 20, "plus2"));
			deck.add(new ActionCard('G', 20, "plus2"));

			// reverse
			deck.add(new ActionCard('R', 20, "rev"));
			deck.add(new ActionCard('B', 20, "rev"));
			deck.add(new ActionCard('Y', 20, "rev"));
			deck.add(new ActionCard('G', 20, "rev"));

			// skip
			deck.add(new ActionCard('R', 20, "skip"));
			deck.add(new ActionCard('B', 20, "skip"));
			deck.add(new ActionCard('Y', 20, "skip"));
			deck.add(new ActionCard('G', 20, "skip"));

		}
		// plus4
		for (int index = 0; index < 4; index++) {
			deck.add(new ActionCard('M', 50, "plus4"));
			deck.add(new ActionCard('M', 50, "multi"));
		}
		Collections.shuffle(deck); // Mix Cards
	}

	/**
	 * test if the Deck is empty. if true the top Card of the CardPile will be
	 * noticed and the rest of the pile will be mixed and will considerate it as
	 * deck
	 * 
	 * @return Card : the picked card from the deck
	 */
	public Card pickUpfromDeck() {
		// if the size of the deck is 0
		if (deck.size() == 0) {
			// notice the topCard of the pile
			Card topCard = cardPile.remove(0);
			// mix the rest of the pile
			Collections.shuffle(cardPile);
			// create a new deck with the mixed pile
			deck.addAll(cardPile);
			// clear the pile
			cardPile.clear();
			// add the topCard to the Pile
			cardPile.add(topCard);
		}
		return deck.remove(0);
	}

	/**
	 * A card will be added to the CardPile
	 * 
	 * @param card
	 *            the card will be played and added to the pile
	 */
	// add a card to the cardPile on the 0 position
	public void throwCard(Card card) {
		cardPile.add(0, card);
	}

	/**
	 * A player will be added to the arayList Players
	 * 
	 * @param player
	 *            a player will be added to the game
	 */
	// to add a player to the round
	public void addPlayer(Player player) {
		players.add(player);
	}

	/**
	 * <b>handRound:</b> this function hands out 7 cards pro player
	 */
	public void handRound() {
		for (int zaehler = 0; zaehler < 7; zaehler++) {
			/**
			 * 7 cards per player
			 */
			for (Player sp : players) {
				Card card = pickUpfromDeck();
				sp.addHandCard(card);
			}
		}
		Card temp = null;

		/**
		 * repeat pickUpfromDeck and add it to the cardPile while it an Action card
		 */
		do {
			/**
			 * pick up a card from the deck
			 */
			temp = pickUpfromDeck();

			/**
			 * add the temp to the top of the cardPile
			 */
			cardPile.add(temp);

		} while (temp instanceof ActionCard);
	}

	/**
	 * <b>Round:</b> to start a round
	 * 
	 * @return boolean true: if the players have cards false: the end of the round
	 *         because one of players has no more cards
	 * @throws IOException
	 *             if problems by reading from the console
	 */
	public boolean Round() throws IOException {

		Player actualPlayer = players.remove(0);
		int indexNextPlayer = 0;// next player is always in the 1 position
		int indexpreviousPlayer = players.size() - 1; /// the prevoius player is always in the last position

		Player nextPlayer = players.get(indexNextPlayer);
		Player previousPlayer = players.get(indexpreviousPlayer);

		actualPlayer.setNextPlayer(nextPlayer);
		previousPlayer.setPreviousPlayer(previousPlayer);

		// Display the cards of the actual Player
		System.out.println("*********************************************************");
		System.out.println("Pile's top card:\n ");
		Card topPileCard = cardPile.get(cardPile.size() - 1);
		System.out.println(topPileCard);

		if (topPileCard instanceof ActionCard && ((((ActionCard) topPileCard).getAction() == "multi")
				|| ((ActionCard) topPileCard).getAction() == "plus4")) {
			System.out.println("The choosed color is " + ((ActionCard) topPileCard).getChoosedColor());
		}

			System.out.println("\t\t\tTurn of: " + actualPlayer.getName()); // display the name of the random player
			System.out.println("*********************************************************");
			actualPlayer.showCards();
		
		/**
		 * the cards of rest of players will be hide
		 */
		for (Player otherPlayer : players) {
			if (!(otherPlayer.getName().equalsIgnoreCase(actualPlayer.getName()))) {
				System.out.println("\t\t\tCards of: " + otherPlayer.getName());
				System.out.println("*********************************************************");
				otherPlayer.hideCards();
			}
		}

		Card ChoosedCard = null;

		ChoosedCard = actualPlayer.playCard(cardPile.get(cardPile.size() - 1));

		if (ChoosedCard == null) {
			ChoosedCard = pickUpfromDeck();
			actualPlayer.addHandCard(ChoosedCard);
			System.out.println("Picked card from Deck:\n" + ChoosedCard);
		}

		char charChoosedColor = ' ';
		if (ChoosedCard instanceof ActionCard && (((ActionCard) ChoosedCard).getAction().equalsIgnoreCase("multi")
				|| ((ActionCard) ChoosedCard).getAction().equalsIgnoreCase("plus4"))) {

			ActionCard choosedActionCard = (ActionCard) ChoosedCard;

			charChoosedColor = actualPlayer.chooseColor(choosedActionCard);
			choosedActionCard.setChoosedColor(charChoosedColor);
			System.out.println("The choosed color is " + charChoosedColor);

		}

		boolean sayUno = actualPlayer.sayUno();
		if ((actualPlayer instanceof Player) && (!sayUno) && ((Player) actualPlayer).getSizeHandCard() == 2
				&& (ChoosedCard.match(topPileCard))) {
			System.out.println("You forgot to say UNO, 2 cards will be added to your hand cards");
			PenalityCards(actualPlayer, 2);
		}

		if (sayUno) {
			ArrayList<Card> handcard = ((Player) actualPlayer).getHandCard();
			for (Card card : handcard) {
				if (((Player) actualPlayer).getSizeHandCard() != 2) {
					System.out.println("\n\t\tWrong time to say UNO\n\t2 Cards will be added to your hand cards");
					PenalityCards(actualPlayer, 2);
					break;
				}
				if ((((Player) actualPlayer).getSizeHandCard() == 1) && (ChoosedCard.match(topPileCard))) {
					System.out.println("\n\t\tUUUUUNNNNNOOOOOO\n");
					break;
				}
				if ((((Player) actualPlayer).getSizeHandCard() == 2) && card.match(topPileCard)) {
					System.out.println("\n\t\tUUUUUNNNNNOOOOOO\n");
					break;
				}
			}
		}

		boolean isValid2Play = ChoosedCard.match(topPileCard);
		Player winner = actualPlayer.getSizeHandCard() == 1 && isValid2Play ? actualPlayer : null;

		if (isValid2Play) {
			System.out.print("You choosed: \n" + ChoosedCard);
			System.out.println();
			System.out.println();
			cardPile.add(ChoosedCard);
			actualPlayer.throwCardfromHandCard(ChoosedCard);
			System.out.println("************");
			System.out.println("Playing card\n" + ChoosedCard);

			if (ChoosedCard instanceof ActionCard && ((ActionCard) ChoosedCard).getAction().equalsIgnoreCase("rev")) {
				reverseList(players);
			}

			if (ChoosedCard instanceof ActionCard && ((ActionCard) ChoosedCard).getAction().equalsIgnoreCase("skip")) {
				players.add(players.size(), actualPlayer);
				actualPlayer = players.remove(indexNextPlayer);
			}

			if (ChoosedCard instanceof ActionCard && ((ActionCard) ChoosedCard).getAction().equalsIgnoreCase("plus2")) {
				PenalityCards(nextPlayer, 2);
				players.add(players.size(), actualPlayer);
				actualPlayer = players.remove(indexNextPlayer);
			}

			if (ChoosedCard instanceof ActionCard && ((ActionCard) ChoosedCard).getAction().equalsIgnoreCase("plus4")) {
				PenalityCards(nextPlayer, 4);
				players.add(players.size(), actualPlayer);
				actualPlayer = players.remove(indexNextPlayer);
			}
		}
		players.add(players.size(), actualPlayer);

		if (winner != null) {
			int score = 0;
			for (Player otherPlayer : players) {
				if (!(otherPlayer.getName().equalsIgnoreCase(winner.getName()))) {
					score += otherPlayer.sumValuesHandCards();
				}
			}
			int newRound = winner.getRound() + 1;
			for (Player player : players) {

				player.setRound(newRound);

			}

			System.out.println("*********************************************************");
			System.out.println("\t\tEnd ROUND: " + newRound + "\t");
			System.out.println("*********************************************************\n");

			System.out.println("the player " + winner.getName() + " won this round: " + score);

			// System.out.println("the score is: " + score + " of the round: " +
			// actualPlayer.getRound() );

			managerDb.addPlayer(winner.getName(), winner.getSession(), newRound, score);

			// add time sleep
			// wait 3 second before start a new round
			 try {
			 TimeUnit.MILLISECONDS.sleep(3000);
			 } catch (InterruptedException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
			 }
			return false;
		}
		// add time sleep
		// wait 1 second before the turn of the next player
		 try {
		 TimeUnit.MILLISECONDS.sleep(1000);
		 } catch (InterruptedException e) {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		 }
		return true;

	}

	/**
	 * <b>controlCardsAmount:</b> to check if the amount of cards is always 108
	 * 
	 * @return int: the total of pilecard, deck's cards and players's cards
	 */
	public int controlCardsAmount() {
		int sum = getSizeCardPile() + getSizeDeck();
		for (Player player : players) {
			sum += player.getSizeHandCard();
		}
		return sum;
	}

	/**
	 * <b>getSizeDeck:</b> to return the size of deck
	 * 
	 * @return int:the deck's size
	 */
	public int getSizeDeck() {
		return deck.size();
	}

	/**
	 * <b>getSizeCardPile:</b> to return the size of pile
	 * 
	 * @return int: the cardpile's size
	 */
	public int getSizeCardPile() {
		return cardPile.size();
	}

	/**
	 * <b>PenalityCards:</b> used to add penalities cards to a player it is used by
	 * plus 4, plus2 cards or when the player forget to say uno or he said uno in
	 * the wrong time
	 * 
	 * @param player:
	 *            take as parameter the player that will be penalised
	 * @param cards:
	 *            take as parameter the number of the cards that will be added to
	 *            the player
	 */

	public void PenalityCards(Player player, int cards) {
		for (int i = 0; i < cards; i++) {
			Card pickedCard = pickUpfromDeck();
			player.addHandCard(pickedCard);
		}
	}

	/**
	 * <b>reverseList:</b> to reverse the turn of playing these method will be used
	 * when the thrown card is a rev card
	 * 
	 * @param players
	 *            a list of players is token as parameter
	 */
	public void reverseList(ArrayList<Player> players) {
		Collections.reverse(players);
	}

	/**
	 * <b>isNewSession:</b> to start a new Session
	 * 
	 * @return true if no player has reached 500 points or false if player has 500
	 *         score and then it is the end of session if the winner is a player a
	 *         menu will be displayed to ask the player if he would like to play a
	 *         new session or if he would like to has an overview of the general
	 *         history of session or his history if the winner is a bot, the session
	 *         will be end and a display of the general history of the session and
	 *         bot's history will be displayed
	 * 
	 */
	public boolean isNewSession() {

		Player playerSession = players.get(0);
		int session = playerSession.getSession();
		ArrayList<HashMap<String, String>> results = managerDb.getHistorybySession(session);
		int maxScore = 0;
		String WinnerName = null;
		for (HashMap<String, String> map : results) {
			int temp = Integer.parseInt(map.get("Score"));
			if (temp > maxScore) {
				maxScore = temp;
				WinnerName = map.get("Player");

			}
		}
		boolean result = true;

		if (maxScore < MAXSCORE) {
			try {
				while (Round()) {
//					System.out.println("There are " + controlCardsAmount() + " cards");

				}
				resetGame();
			} catch (IOException e) {
				System.out.println("problem with the appel of round()");
				e.printStackTrace();
			}
		}
		if (maxScore >= MAXSCORE) {

			System.out.println("\n\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println("\t\tEnd of Session");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.printf("\tThe winner is %s with %d points ", WinnerName, maxScore);
			System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println();
			String line;

			Player xx = null;
			for (Player player : players) {
				if (player.getName().equalsIgnoreCase(WinnerName)) {

					if (player instanceof Bot) {
						xx = (Bot) player;
						if (xx instanceof Bot) {

							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
							System.out.println("\t   GENERAL HISTORY FOR THIS SESSION");
							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

							for (HashMap<String, String> map : results) {
								System.out.println("\t\t " + map.get("Player") + "\t\t" + map.get("Score"));
							}
							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

							System.out.println("\t\t   " + WinnerName + "\'s HISTORY");
							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

							System.out.println("\tSession" + "\t\tRound" + "\t\tScore");
							ArrayList<HashMap<String, String>> results2 = managerDb.getHistorybyPlayer(WinnerName);
							for (HashMap<String, String> map : results2) {
								if(map.get("Round").equalsIgnoreCase("0"))
								{
									continue;
								}
								System.out.println("\t  " + map.get("Session") + "\t\t  " + map.get("Round") + "\t\t  "
										+ map.get("Score"));
							}
							System.out.println("********************************************************\n");
						}
						break;
					}

					else {
						xx = player;
						try {
							do {
								System.out.println("Choose 1, 2, 3 or 4");
								System.out.println("1: Overview of Session");
								System.out.println("2: Player's History");
								System.out.println("3: Would you like to play new Session Y/N?");
								System.out.println("4: EXIT");
								System.out.println("Waiting.........\n");
								line = br.readLine();
								switch (line) {
								case "1":
									System.out.println("************* GENERAL HISTORY FOR THIS SESSION *********");

									for (HashMap<String, String> map : results) {
										System.out.println(
												"\t " + map.get("Player") + "\t\t " + " \t" + map.get("Score"));
									}
									System.out.println("*********************************************************\n");
									break;

								case "2":
									ArrayList<HashMap<String, String>> results2 = managerDb
											.getHistorybyPlayer(WinnerName);
									System.out.println(
											"*********************" + WinnerName + "'s HISTORY *********************");
									System.out.println("\tSession" + "\t\tRound" + "\t\tScore");
									for (HashMap<String, String> map : results2) {
										if(map.get("Round").equalsIgnoreCase("0"))
										{
											continue;
										}
										System.out.println("\t" + map.get("Session") + "\t\t" + map.get("Round")
												+ "\t\t" + map.get("Score"));
									}
									System.out.println("**********************************************************\n");
									break;
								case "3":
									System.out.println("Start new Session");

									xx.setSession(++session);

									for (Player listplayer : players) {
										listplayer.setSession(session);
										listplayer.setRound(0);
										managerDb.addPlayer(listplayer.getName(), listplayer.getSession(),
												listplayer.getRound(), 0);
									}

									resetGame();
									return true;

								case "4":
									System.out.println("Thank you for playing!!");
									break;
								default:
									break;
								}

							} while (!line.matches("3") && !line.matches("4"));

						} catch (IOException e) {
							e.printStackTrace();
						}
						break;
					}
				}

			}
			result = false;
		}

		return result;
	}


	/**
	 * <b>resetGame:</b> to clear the hand cards player, the Deck and the pile then
	 * new deck will be created and 7 cards will be given pro player this method
	 * will be used after every round and every session
	 */
	public void resetGame() {
		for (Player player : players) {
			player.getHandCard().clear();
		}
		deck.clear();
		cardPile.clear();
		CreateDeck();
		handRound();
	}

	public ArrayList<String> getListName() {
		ArrayList<String> result = new ArrayList<>();
		for (Player player : players) {
			result.add(player.getName());
		}
		return result;
	}

}
