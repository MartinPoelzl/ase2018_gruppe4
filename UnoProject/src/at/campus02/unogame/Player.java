package at.campus02.unogame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Player {

	private String name;
	private ArrayList<Card> handCard = new ArrayList<>();
	private int Session;
	private int Round;
	private Player nextPlayer;
	private Player previousPlayer;

	/**
	 * <b>Player</b>: to instance a player with a name
	 * 
	 * @param name:
	 *            the name of the player
	 */
	public Player(String name) {
		this.name = name;
	}

	/**
	 * <b>getRound:</b> returns the actual round of the player
	 * 
	 * @return integer: the actual round
	 * 
	 */
	public int getRound() {
		return Round;
	}

	/**
	 * <b>getNextPlayer:</b> returns the next player
	 * 
	 * @return Player: the next round
	 * 
	 */
	public Player getNextPlayer() {
		return nextPlayer;
	}

	/**
	 * <b>setNextPlayer:</b> to set the next player
	 * 
	 * @param nextPlayer:
	 *            takes the next player as parameter
	 */
	public void setNextPlayer(Player nextPlayer) {
		this.nextPlayer = nextPlayer;
	}

	/**
	 * <b>getPreviousPlayer:</b> returns the previous player
	 * 
	 * @return Player: the previous round
	 */
	public Player getPreviousPlayer() {
		return previousPlayer;
	}

	/**
	 * <b>setPreviousPlayer:</b> to set the previous player
	 * 
	 * @param previousPlayer:
	 *            takes the previous player as parameter
	 */
	public void setPreviousPlayer(Player previousPlayer) {
		this.previousPlayer = previousPlayer;
	}

	/**
	 * <b>setSession:</b> to set the actual session of player
	 * 
	 * @param session:
	 *            takes the session's number
	 */
	public void setSession(int session) {
		Session = session;
	}

	/**
	 * <b>setRound:</b> to set the actual round of player
	 * 
	 * @param round:
	 *            take the round's number
	 */
	public void setRound(int round) {
		Round = round;
	}

	/**
	 * <b>getSession:</b> to return the actual session of the player
	 * 
	 * @return value: returns the session's number
	 */
	public int getSession() {
		return Session;
	}

	/**
	 * <b>addHandCard:</b> to add a card to the hand card of player
	 * 
	 * @param c:
	 *            takes as parameter the card that will be added to the hand card
	 */
	public void addHandCard(Card c) {
		handCard.add(c);
	}

	/**
	 * <b>getHandCard:</b> to return all the player cards
	 * 
	 * @return returns list of hand cards
	 */
	public ArrayList<Card> getHandCard() {
		return handCard;
	}

	/**
	 * <b>getName:</b> to return the name of player
	 * 
	 * @return String returns the player's name
	 */
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Round;
		result = prime * result + Session;
		result = prime * result + ((handCard == null) ? 0 : handCard.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nextPlayer == null) ? 0 : nextPlayer.hashCode());
		result = prime * result + ((previousPlayer == null) ? 0 : previousPlayer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (Round != other.Round)
			return false;
		if (Session != other.Session)
			return false;
		if (handCard == null) {
			if (other.handCard != null)
				return false;
		} else if (!handCard.equals(other.handCard))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nextPlayer == null) {
			if (other.nextPlayer != null)
				return false;
		} else if (!nextPlayer.equals(other.nextPlayer))
			return false;
		if (previousPlayer == null) {
			if (other.previousPlayer != null)
				return false;
		} else if (!previousPlayer.equals(other.previousPlayer))
			return false;
		return true;
	}

	/**
	 * <b>throwCardfromHandCard:</b> to throw a card from the hand card, if it
	 * exists in the hand card
	 * 
	 * @param card:
	 *            takes a card that can be thrown
	 * @return Card the thrown card
	 */
	// throw a hand card if it exist
	public Card throwCardfromHandCard(Card card) {
		int indexofCard = -1;
		if (existingHandCards(card)) {
			indexofCard = handCard.indexOf(card);
			handCard.remove(indexofCard);
			return card;
		} else
			return null;
	}

	/**
	 * <b>sayUno:</b> to ask the player if he want to say UNO
	 * 
	 * @return true or false
	 *         <ul>
	 *         <li>true: if the player has written y</li>
	 *         <li>false: if the player has written n</li>
	 *         </ul>
	 * @throws IOException if problems by reading from the console
	 */
	public boolean sayUno() throws IOException {
		String line;
		boolean result = false;
		do {
			System.out.println("Say UNO Y/N?");
			InputStreamReader ins = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(ins);
			line = br.readLine();
			line = line.toLowerCase();
			if (line.equals("exit")) {
				System.out.println("Thank you for your visit!! See you again!! ");
				System.exit(0);

			}
			if (line.equalsIgnoreCase("y"))
				result = true;
			else
				result = false;

		} while (!line.matches("y") && !line.matches("n"));

		return result;
	}

	/**
	 * <b>playCard:</b> to ask the player which card wants to play and test if the
	 * chosen card matches with the top pile's card
	 * 
	 * @param pileCard:
	 *            takes as parameter the pile card's top
	 * @return Card or null
	 * @throws IOException if problems by reading from the console
	 */
	public Card playCard(Card pileCard) throws IOException {

		Card ChoosedCard = null;
		String CardValue = null;
		do {

			System.out.print(this.getName() + " Choose a Card: ");
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			// Read line from console.
			CardValue = in.readLine();

			if (CardValue.equalsIgnoreCase("exit")) {
				System.out.println("Thank you for your visit!! See you again!! ");
				System.exit(0);
			}

			ChoosedCard = inputAnalyse(CardValue);
			if (ChoosedCard != null && ChoosedCard.match(pileCard) == true && existingHandCards(ChoosedCard)) {
				break;
			}

		} while (!CardValue.equals("") && this.existingHandCards(ChoosedCard) == false);
		return ChoosedCard;
	}

	/**
	 * <b>existingHandCards:</b> to test if the chosen card by the player exists in
	 * the player's hand cards
	 * 
	 * @param cardtoCompare:
	 *            takes as parameter a card that will be compared with the hand
	 *            cards
	 * @return true or false
	 *         <ul>
	 *         <li>true: if it exists</li>
	 *         <li>false: if it is not exist</li>
	 *         </ul>
	 */
	private boolean existingHandCards(Card cardtoCompare) {
		if (cardtoCompare instanceof NumberCard) {
			NumberCard numbercard = (NumberCard) cardtoCompare;
			if (handCard.contains(numbercard))
				return true;
		}
		if (cardtoCompare instanceof ActionCard) {
			ActionCard actionCard = (ActionCard) cardtoCompare;
			if (handCard.contains(actionCard))
				return true;
		}

		return false;
	}

	/**
	 * <b>chooseColor:</b> to let the player choose a color if the playing card is
	 * an Action card
	 * 
	 * @param choosedActionCard:
	 *            takes as parameter an Action card
	 * @return character: the color chosen by the player and set the chosen color of
	 *         the choosedActionCard with the ChoosedColor
	 * @throws IOException
	 *             if problems by reading from the console
	 */
	public char chooseColor(ActionCard choosedActionCard) throws IOException {
		String choosedColor = null;
		char charChoosedColor;
		do {
			System.out.println("Give a color: R, G, B or Y ");
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			// Read line from console.
			choosedColor = in.readLine();
			if (choosedColor.equalsIgnoreCase("exit")) {
				System.out.println("Thank you for your visit!! See you again!! ");
				System.exit(0);
			}
		} while (isRightColor(choosedColor) == false);
		charChoosedColor = choosedColor.toUpperCase().charAt(0);
		choosedActionCard.setChoosedColor(charChoosedColor);
		return charChoosedColor;
	}

	/**
	 * <b>isRightColor:</b> to test the chosen color input of player. it is used to
	 * avoid any wrong input
	 * 
	 * @param color
	 *            takes as parameter a color
	 * @return boolean true or false
	 *         <ul>
	 *         <li>true: if the color is r, b, g or y</li>
	 *         <li>false: if the color not right</li>
	 *         </ul>
	 */
	private boolean isRightColor(String color) {

		if (color.equalsIgnoreCase("r") || color.equalsIgnoreCase("b") || color.equalsIgnoreCase("g")
				|| color.equalsIgnoreCase("y"))
			return true;

		return false;

	}

	/**
	 * <b>inputAnalyse: </b> to analyze an input string if it corresponds to a card
	 * or not (without compare it with the handcard).
	 * 
	 * @param input:
	 *            the input of the player as a chosen card as a parameter
	 * @return Card: returns the corresponding card if the input is right or null if
	 *         the input is wrong
	 */

	public Card inputAnalyse(String input) {
		Card result = null;
		char[] arrayString = input.toCharArray();
		String action = "";
		if (arrayString.length == 2) {
			char color = arrayString[0];
			char colorToUpperCase = Character.toUpperCase(color);
			int value = arrayString[1] - 48;
			ArrayList<Character> inputpossibilty = new ArrayList<>();
			inputpossibilty.add('G');
			inputpossibilty.add('B');
			inputpossibilty.add('R');
			inputpossibilty.add('Y');
			ArrayList<Integer> inputValuepossibilty = new ArrayList<>();
			inputValuepossibilty.add(0);
			inputValuepossibilty.add(1);
			inputValuepossibilty.add(2);
			inputValuepossibilty.add(3);
			inputValuepossibilty.add(4);
			inputValuepossibilty.add(5);
			inputValuepossibilty.add(6);
			inputValuepossibilty.add(7);
			inputValuepossibilty.add(8);
			inputValuepossibilty.add(9);
			if (inputpossibilty.contains(colorToUpperCase) && inputValuepossibilty.contains(value))
				result = new NumberCard(colorToUpperCase, value);
			else
				result = null;
		} else if (arrayString.length > 0) {
			char color = arrayString[0];
			char colorToUpperCase = Character.toUpperCase(color);
			for (int index = 1; index < arrayString.length; index++) {
				action += "" + arrayString[index];
			}
			String actiontoLowerCase = action.toLowerCase();
			if (actiontoLowerCase.equalsIgnoreCase("multi"))
				result = new ActionCard(colorToUpperCase, 50, "multi");
			else if (actiontoLowerCase.equalsIgnoreCase("+4"))
				result = new ActionCard(colorToUpperCase, 50, "plus4");
			else if (actiontoLowerCase.equalsIgnoreCase("+2"))
				result = new ActionCard(colorToUpperCase, 20, "plus2");
			else if (actiontoLowerCase.equalsIgnoreCase("skip") || actiontoLowerCase.equalsIgnoreCase("rev"))
				result = new ActionCard(colorToUpperCase, 20, actiontoLowerCase);
			else
				result = null;
		}
		return result;
	}

	/**
	 * <b>getSizeHandCard:</b> to count the player's hand cards
	 * 
	 * @return integer: the size of the hand card
	 */
	public int getSizeHandCard() {
		return handCard.size();
	}

	/**
	 * <b>sumValuesHandCards:</b> to sum the values of all player's cards
	 * 
	 * @return int the total sum of the player's hand card
	 */
	public int sumValuesHandCards() {
		int sum = 0;
		for (Card card : handCard) {
			sum += card.getValue();
		}
		return sum;
	}

	/**
	 * <b>showCards:</b> graphical display of the player's handcards
	 */
	public void showCards() {

		String[] card = { " ------- ", "|     |", "|     |", " ------- " };
		String c = "";

		for (int i = 0; i < card.length; i++) {

			for (int j = 0; j < handCard.size(); j++) {

				if (!(handCard.get(j) instanceof ActionCard)) {
					if (i == 1) {

						c = c + "|   " + handCard.get(j).getColor() + "   |" + " ";

					}

					else if (i == 2) {

						c = c + "|   " + handCard.get(j).getValue() + "   |" + " ";
					}

					else {
						c = c + card[i] + " ";
					}

				} else if ((handCard.get(j) instanceof ActionCard)) {
					ActionCard x = (ActionCard) handCard.get(j);
					if (i == 1) {

						c = c + "|   " + x.getColor() + "   |" + " ";

					} else if (i == 2) {
						if (x.getAction().equalsIgnoreCase("rev")) {

							c = c + "|  " + x.getAction() + "  |" + " ";
						}

						if (x.getAction().equalsIgnoreCase("skip")) {

							c = c + "| " + x.getAction() + "  |" + " ";
						}
						if (x.getAction().equalsIgnoreCase("multi")) {

							c = c + "| " + x.getAction() + " |" + " ";
						}
						if (x.getAction().equalsIgnoreCase("plus2")) {

							c = c + "|" + "  " + "+2" + "   |" + " ";
						}
						if (x.getAction().equalsIgnoreCase("plus4")) {

							c = c + "|  " + "+4" + "   |" + " ";
						}
					} else {
						c = c + card[i] + " ";
					}
				}
			}
			c += "\n";
		}

		System.out.print(c);
	}

	/**
	 * <b>hideCards:</b> graphical display: to hide the player's cards
	 */
	public void hideCards() {

		String[] card = { " ----- ", "|     |", "|     |", " ----- " };
		String c = "";

		for (int i = 0; i < card.length; i++) {
			for (int j = 0; j < handCard.size(); j++) {
				c = c + card[i] + " ";
			}
			c += "\n";
		}
		System.out.print(c);
	}

}
