package at.campus02.test.unogame;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import at.campus02.unogame.ActionCard;
import at.campus02.unogame.NumberCard;

public class ActionCardTest {

	/**
	 *  Test 1: check match method for wildcard multi with multi
	 */
	@Test
	public void test1() {
		ActionCard c1 = new ActionCard('M', 50, "multi");
		ActionCard c2 = new ActionCard('M', 50, "multi");

		boolean output = c1.match(c2);
		assertEquals(true, output);
	}

	/**
	 * Test 2: check match method for wildcard multi with skip
	 */
	@Test
	public void test2() {
		ActionCard c1 = new ActionCard('M', 50, "multi");

		ActionCard c2 = new ActionCard('R', 20, "skip");
		boolean output = c1.match(c2);
		assertEquals(true, output);

		ActionCard c21 = new ActionCard('B', 20, "skip");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "skip");
		boolean output2 = c1.match(c22);
		assertEquals(true, output2);

		ActionCard c23 = new ActionCard('y', 20, "skip");
		boolean output3 = c1.match(c23);
		assertEquals(true, output3);
	}

	/**
	 * Test 3: check match method for wildcard multi with reverse
	 */
	@Test
	public void test3() {
		ActionCard c1 = new ActionCard('M', 50, "multi");

		ActionCard c2 = new ActionCard('R', 20, "rev");
		boolean output = c1.match(c2);
		assertEquals(true, output);

		ActionCard c21 = new ActionCard('B', 20, "rev");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "rev");
		boolean output2 = c1.match(c22);
		assertEquals(true, output2);

		ActionCard c23 = new ActionCard('y', 20, "rev");
		boolean output3 = c1.match(c23);
		assertEquals(true, output3);
	}

	/**
	 * Test 4: check match method for wildcard multi with plus2
	 */
	@Test
	public void test4() {
		ActionCard c1 = new ActionCard('M', 50, "multi");

		ActionCard c2 = new ActionCard('R', 20, "plus2");
		boolean output = c1.match(c2);
		assertEquals(true, output);

		ActionCard c21 = new ActionCard('B', 20, "plus2");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "plus2");
		boolean output2 = c1.match(c22);
		assertEquals(true, output2);

		ActionCard c23 = new ActionCard('y', 20, "plus2");
		boolean output3 = c1.match(c23);
		assertEquals(true, output3);

	}

	/**
	 * Test 5: check match method for wildcard multi with plus4
	 */
	@Test
	public void test5() {
		ActionCard c1 = new ActionCard('M', 50, "multi");

		ActionCard c2 = new ActionCard('M', 50, "plus4");
		boolean output = c1.match(c2);
		assertEquals(true, output);

	}

	/**
	 * SKIP CARD Test 6: check match method for skip with skip
	 */
	@Test
	public void test6() {
		ActionCard c1 = new ActionCard('B', 20, "skip");

		ActionCard c2 = new ActionCard('B', 20, "skip");
		ActionCard c3 = new ActionCard('G', 20, "skip");
		ActionCard c4 = new ActionCard('R', 20, "skip");
		ActionCard c5 = new ActionCard('Y', 20, "skip");
		boolean output = c1.match(c2);
		boolean output1 = c1.match(c3);
		boolean output2 = c1.match(c4);
		boolean output3 = c1.match(c5);
		assertEquals(true, output);
		assertEquals(true, output1);
		assertEquals(true, output2);
		assertEquals(true, output3);

	}

	/**
	 * Test 7: check match method for skip with multi
	 */
	@Test
	public void test7() {
		ActionCard c1 = new ActionCard('B', 20, "skip");

		ActionCard c2 = new ActionCard('M', 50, "multi");
		c2.setChoosedColor('B');
		boolean output = c1.match(c2);
		assertEquals(true, output);

		c2.setChoosedColor('R');
		boolean output2 = c1.match(c2);
		assertEquals(false, output2);

	}

	/**
	 * Test 8: check match method for skip with reverse
	 */
	@Test
	public void test8() {
		ActionCard c1 = new ActionCard('B', 20, "skip");

		ActionCard c2 = new ActionCard('R', 20, "rev");
		boolean output = c1.match(c2);
		assertEquals(false, output);

		ActionCard c21 = new ActionCard('B', 20, "rev");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "rev");
		boolean output2 = c1.match(c22);
		assertEquals(false, output2);

		ActionCard c23 = new ActionCard('y', 20, "rev");
		boolean output3 = c1.match(c23);
		assertEquals(false, output3);

	}

	/**
	 * Test 9: check match method for skip with plus2
	 */
	@Test
	public void test9() {
		ActionCard c1 = new ActionCard('B', 20, "skip");

		ActionCard c2 = new ActionCard('R', 20, "plus2");
		boolean output = c1.match(c2);
		assertEquals(false, output);

		ActionCard c21 = new ActionCard('B', 20, "plus2");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "plus2");
		boolean output2 = c1.match(c22);
		assertEquals(false, output2);

		ActionCard c23 = new ActionCard('y', 20, "plus2");
		boolean output3 = c1.match(c23);
		assertEquals(false, output3);

	}

	/**
	 * Test 10: check match method for skip with plus4
	 */
	@Test
	public void test10() {
		ActionCard c1 = new ActionCard('B', 50, "skip");

		ActionCard c2 = new ActionCard('M', 50, "plus4");
		c2.setChoosedColor('B');
		boolean output = c1.match(c2);
		assertEquals(true, output);

		c2.setChoosedColor('Y');
		boolean output1 = c1.match(c2);
		assertEquals(false, output1);

	}

	/**
	 * REVERSE CARD Test 11: check match method for reverse with skip
	 */
	@Test
	public void test11() {
		ActionCard c1 = new ActionCard('B', 20, "rev");

		ActionCard c2 = new ActionCard('B', 20, "skip");
		ActionCard c3 = new ActionCard('G', 20, "skip");
		ActionCard c4 = new ActionCard('R', 20, "skip");
		ActionCard c5 = new ActionCard('Y', 20, "skip");
		boolean output = c1.match(c2);
		boolean output1 = c1.match(c3);
		boolean output2 = c1.match(c4);
		boolean output3 = c1.match(c5);
		assertEquals(true, output);
		assertEquals(false, output1);
		assertEquals(false, output2);
		assertEquals(false, output3);

	}

	/**
	 * Test 12: check match method for reverse with multi
	 */
	@Test
	public void test12() {
		ActionCard c1 = new ActionCard('B', 20, "rev");

		ActionCard c2 = new ActionCard('M', 50, "multi");
		c2.setChoosedColor('B');
		boolean output = c1.match(c2);
		assertEquals(true, output);

		c2.setChoosedColor('R');
		boolean output1 = c1.match(c2);
		assertEquals(false, output1);
	}

	/**
	 * Test 13: check match method for reverse with reverse
	 */
	@Test
	public void test13() {
		ActionCard c1 = new ActionCard('B', 20, "rev");

		ActionCard c2 = new ActionCard('R', 20, "rev");
		boolean output = c1.match(c2);
		assertEquals(true, output);

		ActionCard c21 = new ActionCard('B', 20, "rev");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "rev");
		boolean output2 = c1.match(c22);
		assertEquals(true, output2);

		ActionCard c23 = new ActionCard('y', 20, "rev");
		boolean output3 = c1.match(c23);
		assertEquals(true, output3);

	}

	/**
	 * Test 14: check match method for reverse with plus2
	 */
	@Test
	public void test14() {
		ActionCard c1 = new ActionCard('B', 20, "rev");

		ActionCard c2 = new ActionCard('R', 20, "plus2");
		boolean output = c1.match(c2);
		assertEquals(false, output);

		ActionCard c21 = new ActionCard('B', 20, "plus2");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "plus2");
		boolean output2 = c1.match(c22);
		assertEquals(false, output2);

		ActionCard c23 = new ActionCard('y', 20, "plus2");
		boolean output3 = c1.match(c23);
		assertEquals(false, output3);

	}

	/**
	 * Test 15: check match method for reverse with plus4
	 */
	@Test
	public void test15() {
		ActionCard c1 = new ActionCard('B', 50, "skip");

		ActionCard c2 = new ActionCard('M', 50, "plus4");
		c2.setChoosedColor('B');
		boolean output = c1.match(c2);
		assertEquals(true, output);

		c2.setChoosedColor('Y');
		boolean output1 = c1.match(c2);
		assertEquals(false, output1);
	}

	/**
	 * PLUS2 Test 16: check match method for plus2 with skip
	 */
	@Test
	public void test16() {
		ActionCard c1 = new ActionCard('B', 20, "plus2");

		ActionCard c2 = new ActionCard('B', 20, "skip");
		ActionCard c3 = new ActionCard('G', 20, "skip");
		ActionCard c4 = new ActionCard('R', 20, "skip");
		ActionCard c5 = new ActionCard('Y', 20, "skip");
		boolean output = c1.match(c2);
		boolean output1 = c1.match(c3);
		boolean output2 = c1.match(c4);
		boolean output3 = c1.match(c5);
		assertEquals(true, output);
		assertEquals(false, output1);
		assertEquals(false, output2);
		assertEquals(false, output3);

	}

	/**
	 * Test 17: check match method for plus2 with multi
	 */
	@Test
	public void test17() {
		ActionCard c1 = new ActionCard('B', 20, "plus2");

		ActionCard c2 = new ActionCard('M', 50, "multi");
		c2.setChoosedColor('B');
		boolean output = c1.match(c2);
		assertEquals(true, output);

		c2.setChoosedColor('G');
		boolean output1 = c1.match(c2);
		assertEquals(false, output1);

	}

	/**
	 * Test 18: check match method for plus2 with reverse
	 */
	@Test
	public void test18() {
		ActionCard c1 = new ActionCard('B', 20, "plus2");

		ActionCard c2 = new ActionCard('R', 20, "rev");
		boolean output = c1.match(c2);
		assertEquals(false, output);

		ActionCard c21 = new ActionCard('B', 20, "rev");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "rev");
		boolean output2 = c1.match(c22);
		assertEquals(false, output2);

		ActionCard c23 = new ActionCard('y', 20, "rev");
		boolean output3 = c1.match(c23);
		assertEquals(false, output3);

	}

	/**
	 * Test 19: check match method for plus2 with plus2
	 */
	@Test
	public void test19() {
		ActionCard c1 = new ActionCard('B', 20, "plus2");

		ActionCard c2 = new ActionCard('R', 20, "plus2");
		boolean output = c1.match(c2);
		assertEquals(true, output);

		ActionCard c21 = new ActionCard('B', 20, "plus2");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "plus2");
		boolean output2 = c1.match(c22);
		assertEquals(true, output2);

		ActionCard c23 = new ActionCard('y', 20, "plus2");
		boolean output3 = c1.match(c23);
		assertEquals(true, output3);

	}

	/**
	 * Test 20: check match method for plus2 with plus4
	 */
	@Test
	public void test20() {
		ActionCard c1 = new ActionCard('B', 20, "plus2");

		ActionCard c2 = new ActionCard('M', 50, "plus4");
		c2.setChoosedColor('B');
		boolean output = c1.match(c2);
		assertEquals(true, output);

		c2.setChoosedColor('Y');
		boolean output1 = c1.match(c2);
		assertEquals(false, output1);
	}

	/**
	 * PLUS4 Test 21: check match method for plus4 with skip
	 */

	@Test
	public void test21() {
		ActionCard c1 = new ActionCard('M', 50, "plus4");

		ActionCard c2 = new ActionCard('B', 20, "skip");
		ActionCard c3 = new ActionCard('G', 20, "skip");
		ActionCard c4 = new ActionCard('R', 20, "skip");
		ActionCard c5 = new ActionCard('Y', 20, "skip");
		boolean output = c1.match(c2);
		boolean output1 = c1.match(c3);
		boolean output2 = c1.match(c4);
		boolean output3 = c1.match(c5);
		assertEquals(true, output);
		assertEquals(true, output1);
		assertEquals(true, output2);
		assertEquals(true, output3);

	}

	/**
	 * Test 22: check match method for plus4 with multi
	 */
	@Test
	public void test22() {
		ActionCard c1 = new ActionCard('M', 50, "plus4");

		ActionCard c2 = new ActionCard('M', 50, "multi");
		boolean output = c1.match(c2);
		assertEquals(true, output);

	}

	/**
	 * Test 23: check match method for plus4 with reverse
	 */
	@Test
	public void test23() {
		ActionCard c1 = new ActionCard('M', 50, "plus4");

		ActionCard c2 = new ActionCard('R', 20, "rev");
		boolean output = c1.match(c2);
		assertEquals(true, output);

		ActionCard c21 = new ActionCard('B', 20, "rev");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "rev");
		boolean output2 = c1.match(c22);
		assertEquals(true, output2);

		ActionCard c23 = new ActionCard('y', 20, "rev");
		boolean output3 = c1.match(c23);
		assertEquals(true, output3);

	}

	/**
	 * Test 24: check match method for plus4 with plus2
	 */
	@Test
	public void test24() {
		ActionCard c1 = new ActionCard('M', 50, "plus4");

		ActionCard c2 = new ActionCard('R', 20, "plus2");
		boolean output = c1.match(c2);
		assertEquals(true, output);

		ActionCard c21 = new ActionCard('B', 20, "plus2");
		boolean output1 = c1.match(c21);
		assertEquals(true, output1);

		ActionCard c22 = new ActionCard('G', 20, "plus2");
		boolean output2 = c1.match(c22);
		assertEquals(true, output2);

		ActionCard c23 = new ActionCard('y', 20, "plus2");
		boolean output3 = c1.match(c23);
		assertEquals(true, output3);

	}

	/**
	 * Test 25: check match method for plus4 with plus4
	 */
	@Test
	public void test25() {
		ActionCard c1 = new ActionCard('M', 50, "plus4");

		ActionCard c2 = new ActionCard('M', 50, "plus4");
		boolean output = c1.match(c2);
		assertEquals(true, output);

	}

	/**
	 * ACTION CARD with NUMBER CARD Test 26: check match method for multi with
	 * number card
	 */
	@Test
	public void test26() {
		ActionCard c1 = new ActionCard('M', 50, "multi");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

	}

	/**
	 * Test 27: check match method for plus4 with number card
	 */
	@Test
	public void test27() {
		ActionCard c1 = new ActionCard('M', 50, "plus4");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

	}

	/**
	 * Test 28: check match method for skip with number card
	 */
	@Test
	public void test28() {
		// Bskip
		ActionCard c1 = new ActionCard('B', 20, "skip");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}

		// Gskip
		ActionCard c11 = new ActionCard('G', 20, "skip");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c11.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}
		// Rskip
		ActionCard c12 = new ActionCard('R', 20, "skip");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c12.match(c2);
			assertEquals(true, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}
		// Yskip
		ActionCard c13 = new ActionCard('Y', 20, "skip");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c13.match(c2);
			assertEquals(true, output);
		}

	}

	/**
	 * Test 29: check match method for reverse with number card
	 */
	@Test
	public void test29() {
		// Brev
		ActionCard c1 = new ActionCard('B', 20, "rev");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}

		// Grev
		ActionCard c11 = new ActionCard('G', 20, "rev");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c11.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}
		// Rrev
		ActionCard c12 = new ActionCard('R', 20, "rev");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c12.match(c2);
			assertEquals(true, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}
		// Yrev
		ActionCard c13 = new ActionCard('Y', 20, "rev");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c13.match(c2);
			assertEquals(true, output);
		}

	}

	/**
	 * Test 30: check match method for plus2 with number card
	 */
	@Test
	public void test30() {
		// Bplus2
		ActionCard c1 = new ActionCard('B', 20, "plus2");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c1.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c1.match(c2);
			assertEquals(false, output);
		}

		// Gplus2
		ActionCard c11 = new ActionCard('G', 20, "plus2");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c11.match(c2);
			assertEquals(true, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c11.match(c2);
			assertEquals(false, output);
		}
		// Rplus2
		ActionCard c12 = new ActionCard('R', 20, "plus2");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c12.match(c2);
			assertEquals(true, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c12.match(c2);
			assertEquals(false, output);
		}
		// Yplus2
		ActionCard c13 = new ActionCard('Y', 20, "plus2");

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('G', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('B', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}

		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('R', i);
			boolean output = c13.match(c2);
			assertEquals(false, output);
		}
		for (int i = 0; i < 10; i++) {
			NumberCard c2 = new NumberCard('Y', i);
			boolean output = c13.match(c2);
			assertEquals(true, output);
		}
	}
}
