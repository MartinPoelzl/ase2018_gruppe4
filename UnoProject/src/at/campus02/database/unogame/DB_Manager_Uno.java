package at.campus02.database.unogame;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DB_Manager_Uno {

	private static final String CREATETABLE = "CREATE TABLE Sessions (Player varchar(100) NOT NULL, Session int NOT NULL, Round int NOT NULL, Score int NOT NULL, CONSTRAINT PK_Sessions PRIMARY KEY (Player,Session,Round));";
	private static final String INSERT_PLAYER = "INSERT INTO Sessions (Player, Session, Round, Score) VALUES ('%1s', %2d, %3d, %4d);";
	private static final String SELECT_ALL_BYSESSION = "SELECT Player, SUM(Score) AS Score FROM Sessions WHERE Session = %1d GROUP BY Player;";
	private static final String SELECT_ALL_BYPLAYERNAME = "SELECT  Session, Round, Score FROM Sessions WHERE Player = '%1s';";
	private DB_Client_Uno client = null;

	/**
	 * <b>DB_Manager_Uno</b> create a new database with the specific name and the
	 * table with the specific table's name
	 */
	public DB_Manager_Uno() {
		try {
			client = new DB_Client_Uno("UNO_DataBase.sqlite");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			if (client.tableExists("Sessions")) {
				client.executeStatement("DROP TABLE Sessions;");
			}
			client.executeStatement(CREATETABLE);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * <b>addPlayer:</b> to register a player in the data base
	 * 
	 * @param name
	 *            player added in the database has a string name
	 * @param Session
	 *            for every player that is added to the database, an integer session
	 *            can be added
	 * @param Round
	 *            for every player that is added to the database, an integer round
	 *            can be added
	 * @param Score
	 *            for every player that is added to the database, an integer score
	 *            can be added
	 */

	public void addPlayer(String name, int Session, int Round, int Score) {
		try {
			client.executeStatement(String.format(INSERT_PLAYER, name, Session, Round, Score));
			// System.out.println("player: " + name + " has been registred in the Uno's
			// Database\n");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("problem with registration");
		}

	}

	/**
	 * <b>getHistorybySession:</b> to return the sum of score pro player of specific
	 * session
	 * 
	 * @param Session
	 *            is the parameter to select the history in the database by session
	 * @return results are the results saved in an ArrayList, the general history
	 *         per session is returned
	 */
	public ArrayList<HashMap<String, String>> getHistorybySession(int Session) {

		ArrayList<HashMap<String, String>> results = null;
		try {
			results = client.executeQuery(String.format(SELECT_ALL_BYSESSION, Session));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return results;
	}

	/**
	 * <b>getHistorybyPlayer:</b> to give a general overview of player's round and
	 * player'session
	 * 
	 * @param Player
	 *            is the parameter to select the history in the database by player
	 * @return are the results saved in an ArrayList, the general history per player
	 *         is returned
	 */
	public ArrayList<HashMap<String, String>> getHistorybyPlayer(String Player) {

		ArrayList<HashMap<String, String>> results = null;
		try {
			results = client.executeQuery(String.format(SELECT_ALL_BYPLAYERNAME, Player));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return results;
	}

	/**
	 * <b>getWinner:</b> to return the max score by session
	 * 
	 * @param Session
	 *            is the parameter to select the winner per session in the database
	 * @return nameWinner gives the winner's name
	 */
	public String getWinner(int Session) {
		int maxScore = 0;
		String nameWinner = null;

		for (HashMap<String, String> map : getHistorybySession(Session)) {
			int temp = Integer.parseInt(map.get("Score"));
			if (temp > maxScore) {
				maxScore = temp;
				nameWinner = map.get("Player");

			}
		}
		return nameWinner;
	}
}
