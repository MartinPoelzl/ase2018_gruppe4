package at.campus02.unogame;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Readme {

	/**
	 * to read an external file and return the content of the file on the console
	 */
	public Readme() {
		try {

			InputStream fis = new FileInputStream("Readme.rd");
			BufferedReader isr = new BufferedReader(new InputStreamReader(fis));
			String line;
			while ((line = isr.readLine()) != null) {
				System.out.println(line + "\n");
			}

			isr.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
