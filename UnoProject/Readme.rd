~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
INSTRUCTIONS:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

There are two types of cards:
Action Cards:
	* Reverse:  switch to counterclockwise or vice versa.
	* Skip:  When a player places this card, the next player has to skip their turn.
	* Draw Two(Plus2) � When a person places this card, the next player will have to pick up two cards and forfeit his/her turn.
	* Wild � This card represents all four colors, and can be placed on any card.
	  The player has to state which the color(by writing r,b, g or y) it will represent for the next player. 
	* Wild Draw Four: This acts just like the wild card except that the next player also has to draw four cards as well as forfeit his/her turn. 
	  With this card, you must have no other alternative cards to play that matches the color of the card previously played.
Number Cards:
	* Yellow cards from 0 to 9
	* Red cards from 0 to 9
	* Blue cards from 0 to 9
	* Green cards from 0 to 9

The deck consists of 108 cards, of which there are 25 of each color (red, green, blue, and yellow), each color having two of each rank except
 zero. The ranks in each color are 0 to 9, "Skip", "Draw Two", and "Reverse".In addition, the deck contains 4 each of "Wild" and "Wild Draw Four" cards.

RULES:
* Every player starts with seven cards, and they are dealt face down.
* The rest of the cards are placed in a Draw Pile face down. 
* The top card should be placed in the Discard Pile, and the game begins!
* The first player is normally the player. 
* Every player views his/her cards and tries to match the card in the Discard Pile.
* the player has to match the top pile's card either by the number, color, or the symbol/Action.
  For instance, if the Discard Pile has a red card that is an 8 you have to place either a red card or a card with an 8 on it. 
  The player can select the card by writing on the console. For example a number card it is identified by first character of color and value,
  r7,y2,b9,g0 and the action cards are represented as following: first color of color and action: Reverse card :grev
  b+2, m+4, mmulti, yskip.
* If the player has chosen a card, that's not matching with the top pile's card, he will loose his turn.
* When the player has no matching card he can pick up a card from the deck by using enter.
* to say UNO the player need only to write y  
* When the player throw a Wild Draw Four(+4) or wild card(multi), he needs to choose a color by writing: r, b, y or g.
* When a player has no more hand cards, it is the the end of the round.
* One session has 1 or more rounds until one player reach 500 points.
* A round is off when one of the player is out of cards.

PENALITIES	:
* When the player has 2 cards and one of them matches with the top pile's card and he forgot to write y (say Uno), he has to draw two cards.
* when the player has more than 1 card and he write y (say Uno), he has to draw two cards.

SCORING:
* When a player is out of cards, he gets points for cards left in the opponent's hands as follows:

Number Card = the value of the Card 
Draw two = 20 points
Reverse = 20 points
Skip = 20 points
Wild = 50 points
Wild Draw 4 = 50 points

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
