package at.campus02.unogame;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Start4Bots {

	/**
	 * <b>start:</b> the head will be displayed and the player has to choose to
	 * start the game or to read game instructions (by reading an external file
	 * readme.rd)
	 * 
	 * @throws IOException
	 *             if there is a problem by reading the input of player
	 */
	public void start() throws IOException {
		new Head_Design();
		System.out.println("Welcome to UNO Game");
		System.out.println(
				"\n* to start the game write \"start\"\n* to read the game instructions write \"help\" \n* to exit the game write \"exit\"\n");
		String line;
		do {

			System.out.println("choose.......");
			line = readFromConsole();
		} while (!line.matches("start") && !line.matches("help") && !line.matches("exit"));

		switch (line) {
		case "start":
			play();
			break;
		case "help":
			try {
				InputStream fis = new FileInputStream("Readme.rd");
				BufferedReader isr = new BufferedReader(new InputStreamReader(fis));
				String line2;
				while ((line2 = isr.readLine()) != null) {
					System.out.println(line2 + "\n");
				}
				isr.close();
				menu();
				break;

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		case "exit":
			System.out.println("Thank you for your visit!! See you again!! ");
			break;
		}
	}

	/**
	 * <b<play:</b> to start the game by adding player to the game and hand out 7
	 * cards pro player
	 */
	private void play() {
		UnoGame_Manager game = new UnoGame_Manager();
		game.addBotToGame(4);
		game.handRound();

		while (game.isNewSession()) {

		}
	}

	/**
	 * <b>readFromConsole:</b> to read from the console the player's input
	 * 
	 * @return String what the user has written
	 * @throws IOException
	 *             if there is a problem by reading from console
	 */
	private String readFromConsole() throws IOException {
		String line;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		line = br.readLine();
		line = line.toLowerCase();
		return line;
	}

	/**
	 * <b>menu:</b> to display a menu to player after reading instructions if he
	 * would like to start the game or exit
	 * 
	 * @throws IOException
	 *             if there is a problem by reading from console
	 */
	private void menu() throws IOException {

		System.out.println("\n* to start the game write \"start\" \n* to exit the game write \"exit\"\n");
		String line;
		do {

			System.out.println("choose.......");
			line = readFromConsole();
		}

		while (!line.matches("start") && !line.matches("exit"));
		switch (line) {
		case "start":

			play();

			break;

		case "exit":
			System.out.println("Thank you for your visit!! See you again!! ");
			break;
		}
	}

}
