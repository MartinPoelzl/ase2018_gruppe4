package at.campus02.test.unogame;

import static org.junit.Assert.*;
import org.junit.Test;
import at.campus02.unogame.ActionCard;
import at.campus02.unogame.NumberCard;

public class NumberCardTest {

	/**
	 * Test 1: number card B with number card and action card
	 */
	@Test
	public void test1() {

		for (int i = 0; i < 10; i++)

		{
			NumberCard c1 = new NumberCard('B', i);
			// B0-9 auf B0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('B', i);

				boolean output = c1.match(c2);
				assertEquals(true, output);
			}

			// B0-9 auf R0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('R', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// B0-9 auf G0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('G', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// B0-9 auf Y0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('Y', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// B0-9 mit Rskip
			ActionCard c2 = new ActionCard('R', 20, "skip");
			boolean output = c1.match(c2);
			assertEquals(false, output);
			// b0-9 mit Bskip
			ActionCard c21 = new ActionCard('B', 20, "skip");
			boolean output1 = c1.match(c21);
			assertEquals(true, output1);

			// b0-9 mit Yskip
			ActionCard c22 = new ActionCard('Y', 20, "skip");
			boolean output2 = c1.match(c22);
			assertEquals(false, output2);

			// B0-9 mit Gskip
			ActionCard c23 = new ActionCard('G', 20, "skip");
			boolean output3 = c1.match(c23);
			assertEquals(false, output3);

			// b0-9 mit Grev
			ActionCard c24 = new ActionCard('G', 20, "rev");
			boolean output4 = c1.match(c24);
			assertEquals(false, output4);

			// B0-9 mit Rrev
			ActionCard c25 = new ActionCard('R', 20, "rev");
			boolean output5 = c1.match(c25);
			assertEquals(false, output5);
			// b0-9 mit Brev
			ActionCard c26 = new ActionCard('B', 20, "rev");
			boolean output6 = c1.match(c26);
			assertEquals(true, output6);

			// b0-9 mit Yrev
			ActionCard c27 = new ActionCard('Y', 20, "rev");
			boolean output7 = c1.match(c27);
			assertEquals(false, output7);
			//// plus2
			// B0-9 auf Bplus2
			ActionCard c28 = new ActionCard('B', 20, "plus2");
			boolean output8 = c1.match(c28);
			assertEquals(true, output8);

			// b0-9 mit Gplus2
			ActionCard c29 = new ActionCard('G', 20, "plus2");
			boolean output9 = c1.match(c29);
			assertEquals(false, output9);

			// B0-9 mit Rplus2
			ActionCard c30 = new ActionCard('R', 20, "plus2");
			boolean output10 = c1.match(c30);
			assertEquals(false, output10);

			// b0-9 mit Yplus2
			ActionCard c31 = new ActionCard('Y', 20, "plus2");
			boolean output11 = c1.match(c31);
			assertEquals(false, output11);

			// plus4
			// B0-9 auf plus4
			ActionCard c32 = new ActionCard('M', 50, "plus4");
			c32.setChoosedColor('B');
			boolean output12 = c1.match(c32);
			assertEquals(true, output12);

			ActionCard c34 = new ActionCard('M', 50, "plus4");
			c32.setChoosedColor('b');
			boolean output14 = c1.match(c34);
			assertEquals(false, output14);

			// multi
			// B0-9 auf multi
			ActionCard c33 = new ActionCard('M', 50, "multi");
			boolean output13 = c1.match(c33);
			assertEquals(false, output13);

		}
	}

	/**
	 * Test 2: number card R with number card and action card
	 */
	@Test
	public void test2() {

		for (int i = 0; i < 10; i++)

		{
			NumberCard c1 = new NumberCard('R', i);
			// R0-9 auf R0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('R', i);

				boolean output = c1.match(c2);
				assertEquals(true, output);
			}

			// R0-9 auf B0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('B', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// R0-9 auf G0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('G', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// R0-9 auf Y0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('Y', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// R0-9 mit Rskip
			ActionCard c2 = new ActionCard('R', 20, "skip");
			boolean output = c1.match(c2);
			assertEquals(true, output);
			// R0-9 mit Bskip
			ActionCard c21 = new ActionCard('B', 20, "skip");
			boolean output1 = c1.match(c21);
			assertEquals(false, output1);

			// R0-9 mit Yskip
			ActionCard c22 = new ActionCard('Y', 20, "skip");
			boolean output2 = c1.match(c22);
			assertEquals(false, output2);

			// R0-9 mit Gskip
			ActionCard c23 = new ActionCard('G', 20, "skip");
			boolean output3 = c1.match(c23);
			assertEquals(false, output3);

			// R0-9 mit Grev
			ActionCard c24 = new ActionCard('G', 20, "rev");
			boolean output4 = c1.match(c24);
			assertEquals(false, output4);

			// R0-9 mit Rrev
			ActionCard c25 = new ActionCard('R', 20, "rev");
			boolean output5 = c1.match(c25);
			assertEquals(true, output5);
			// R0-9 mit Brev
			ActionCard c26 = new ActionCard('B', 20, "rev");
			boolean output6 = c1.match(c26);
			assertEquals(false, output6);

			// R0-9 mit Yrev
			ActionCard c27 = new ActionCard('Y', 20, "rev");
			boolean output7 = c1.match(c27);
			assertEquals(false, output7);
			//// plus2
			// R0-9 auf Bplus2
			ActionCard c28 = new ActionCard('B', 20, "plus2");
			boolean output8 = c1.match(c28);
			assertEquals(false, output8);

			// R0-9 mit Gplus2
			ActionCard c29 = new ActionCard('G', 20, "plus2");
			boolean output9 = c1.match(c29);
			assertEquals(false, output9);

			// R0-9 mit Rplus2
			ActionCard c30 = new ActionCard('R', 20, "plus2");
			boolean output10 = c1.match(c30);
			assertEquals(true, output10);

			// R0-9 mit Yplus2
			ActionCard c31 = new ActionCard('Y', 20, "plus2");
			boolean output11 = c1.match(c31);
			assertEquals(false, output11);

			// plus4
			// R0-9 auf plus4
			ActionCard c32 = new ActionCard('M', 50, "plus4");
			boolean output12 = c1.match(c32);
			assertEquals(false, output12);

			// multi
			// R0-9 auf multi
			ActionCard c33 = new ActionCard('M', 50, "multi");
			boolean output13 = c1.match(c33);
			assertEquals(false, output13);
		}
	}

	/**
	 * Test 3: number card G with number card and action card
	 */
	@Test
	public void test3() {

		for (int i = 0; i < 10; i++)

		{
			NumberCard c1 = new NumberCard('G', i);
			// G0-9 auf G0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('G', i);

				boolean output = c1.match(c2);
				assertEquals(true, output);
			}

			// G0-9 auf R0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('R', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// G0-9 auf B0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('B', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// G0-9 auf Y0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('Y', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// G0-9 mit Rskip
			ActionCard c2 = new ActionCard('R', 20, "skip");
			boolean output = c1.match(c2);
			assertEquals(false, output);
			// G0-9 mit Bskip
			ActionCard c21 = new ActionCard('B', 20, "skip");
			boolean output1 = c1.match(c21);
			assertEquals(false, output1);

			// G0-9 mit Yskip
			ActionCard c22 = new ActionCard('Y', 20, "skip");
			boolean output2 = c1.match(c22);
			assertEquals(false, output2);

			// G0-9 mit Gskip
			ActionCard c23 = new ActionCard('G', 20, "skip");
			boolean output3 = c1.match(c23);
			assertEquals(true, output3);

			// G0-9mit Grev
			ActionCard c24 = new ActionCard('G', 20, "rev");
			boolean output4 = c1.match(c24);
			assertEquals(true, output4);

			// G0-9 mit Rrev
			ActionCard c25 = new ActionCard('R', 20, "rev");
			boolean output5 = c1.match(c25);
			assertEquals(false, output5);
			// G0-9 mit Brev
			ActionCard c26 = new ActionCard('B', 20, "rev");
			boolean output6 = c1.match(c26);
			assertEquals(false, output6);

			// G0-9 mit Yrev
			ActionCard c27 = new ActionCard('Y', 20, "rev");
			boolean output7 = c1.match(c27);
			assertEquals(false, output7);
			//// plus2
			// G0-9 auf Bplus2
			ActionCard c28 = new ActionCard('B', 20, "plus2");
			boolean output8 = c1.match(c28);
			assertEquals(false, output8);

			// G0-9 mit Gplus2
			ActionCard c29 = new ActionCard('G', 20, "plus2");
			boolean output9 = c1.match(c29);
			assertEquals(true, output9);

			// G0-9 mit Rplus2
			ActionCard c30 = new ActionCard('R', 20, "plus2");
			boolean output10 = c1.match(c30);
			assertEquals(false, output10);

			// G0-9 mit Yplus2
			ActionCard c31 = new ActionCard('Y', 20, "plus2");
			boolean output11 = c1.match(c31);
			assertEquals(false, output11);

			// plus4
			// G0-9 auf plus4
			ActionCard c32 = new ActionCard('M', 50, "plus4");
			boolean output12 = c1.match(c32);
			assertEquals(false, output12);

			// multi
			// G0-9 auf multi
			ActionCard c33 = new ActionCard('M', 50, "multi");
			boolean output13 = c1.match(c33);
			assertEquals(false, output13);

		}
	}

	/**
	 * Test 4: number card Y with number card and action card
	 */
	@Test
	public void test4() {

		for (int i = 0; i < 10; i++)

		{
			NumberCard c1 = new NumberCard('Y', i);
			// Y0-9 auf Y0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('Y', i);

				boolean output = c1.match(c2);
				assertEquals(true, output);
			}

			// Y0-9 auf R0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('R', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// Y0-9 auf G0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('G', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}

			// Y0-9 auf B0-9
			for (int j = 0; j < 10; j++) {

				NumberCard c2 = new NumberCard('Y', i);

				boolean output = c1.match(c2);
				if (c1.getValue() == c2.getValue())
					assertEquals(true, output);
				else
					assertEquals(false, output);
			}
			// Y0-9 mit Rskip
			ActionCard c2 = new ActionCard('R', 20, "skip");
			boolean output = c1.match(c2);
			assertEquals(false, output);
			// Y0-9 mit Bskip
			ActionCard c21 = new ActionCard('B', 20, "skip");
			boolean output1 = c1.match(c21);
			assertEquals(false, output1);

			// Y0-9 mit Yskip
			ActionCard c22 = new ActionCard('Y', 20, "skip");
			boolean output2 = c1.match(c22);
			assertEquals(true, output2);

			// Y0-9 mit Gskip
			ActionCard c23 = new ActionCard('G', 20, "skip");
			boolean output3 = c1.match(c23);
			assertEquals(false, output3);

			// Y0-9mit Grev
			ActionCard c24 = new ActionCard('G', 20, "rev");
			boolean output4 = c1.match(c24);
			assertEquals(false, output4);

			// Y0-9 mit Rrev
			ActionCard c25 = new ActionCard('R', 20, "rev");
			boolean output5 = c1.match(c25);
			assertEquals(false, output5);
			// Y0-9 mit Brev
			ActionCard c26 = new ActionCard('B', 20, "rev");
			boolean output6 = c1.match(c26);
			assertEquals(false, output6);

			// Y0-9 mit Yrev
			ActionCard c27 = new ActionCard('Y', 20, "rev");
			boolean output7 = c1.match(c27);
			assertEquals(true, output7);
			//// plus2
			// Y0-9 auf Bplus2
			ActionCard c28 = new ActionCard('B', 20, "plus2");
			boolean output8 = c1.match(c28);
			assertEquals(false, output8);

			// Y0-9 mit Gplus2
			ActionCard c29 = new ActionCard('G', 20, "plus2");
			boolean output9 = c1.match(c29);
			assertEquals(false, output9);

			// Y0-9 mit Rplus2
			ActionCard c30 = new ActionCard('R', 20, "plus2");
			boolean output10 = c1.match(c30);
			assertEquals(false, output10);

			// Y0-9 mit Yplus2
			ActionCard c31 = new ActionCard('Y', 20, "plus2");
			boolean output11 = c1.match(c31);
			assertEquals(true, output11);

			// plus4
			// Y0-9 auf plus4
			ActionCard c32 = new ActionCard('M', 50, "plus4");
			boolean output12 = c1.match(c32);
			assertEquals(false, output12);

			// multi
			// Y0-9 auf multi
			ActionCard c33 = new ActionCard('M', 50, "multi");
			boolean output13 = c1.match(c33);
			assertEquals(false, output13);

		}
	}
}
