package at.campus02.unogame;

public class ActionCard extends Card {

	private String action;
	private char choosedColor;

	/**
	 * <b> ActionCard </b>: to instance an action card
	 * 
	 * @param color
	 *            take the color of card as parameter
	 * @param value
	 *            take the value of card as parameter
	 * @param action
	 *            take the action of card as parameter <b>color</b> defines the
	 *            color of the card
	 *            <ul>
	 *            <li>y, r, b, g: to identify a skip, plus2 or rev card's color</li>
	 *            <li>m: to identify a plus4 card's color or multi card'color</li>
	 *            </ul>
	 * @param value
	 *            take the value of the card as parameter
	 *            <ul>
	 *            <li>20: value of skip, plus2 and rev cards</li>
	 *            <li>50: value of plus4 and multi cards</li>
	 *            </ul>
	 * @param action
	 *            take the action of the card as parameter
	 *            <ul>
	 *            <li>skip, rev, plus2, plus4 or mmulti</li>
	 *            </ul>
	 */
	public ActionCard(char color, int value, String action) {
		super(color, value);
		this.action = action;
	}

	/**
	 * <b>getAction</b> returns the action of a card
	 * 
	 * @return String: plus2, plus4, multi, rev or skip
	 */
	public String getAction() {
		return action;
	}

	/**
	 * <b>setChoosedColor</b> is used to set the chosen color of a card it is only
	 * used, if the action card is a plus4 or multi
	 * 
	 * @param choosedColor:
	 *            takes as parameter a character as a chosen color by the player
	 */
	public void setChoosedColor(char choosedColor) {
		this.choosedColor = choosedColor;
	}

	/**
	 * <b> getChoosedColor</b> returns the chosen color by the player if the card is
	 * plus4 or multi card
	 * 
	 * @return char: A character will be returned: r, b, g or y
	 */
	public char getChoosedColor() {
		return choosedColor;
	}

	/**
	 * <b> match</b> is used to compare the action card with the other card
	 * 
	 * @param card
	 *            used by comparing
	 * @return boolean returns true or false
	 *         <ul>
	 *         <li>true: if the action card matches with the card</li>
	 *         <li>false: if the action card doesn't match with the card
	 *         </ul>
	 */
	@Override
	public boolean match(Card card) {
		// compare the Action card with number card
		if (card instanceof NumberCard) {
			if (this.getColor() == card.getColor() || this.getAction() == "plus4" || this.getAction() == "multi")
				return true;
		} else if (card instanceof ActionCard) {
			ActionCard card2 = (ActionCard) card;
			// compare reverse card with other action card
			if (this.getAction().equalsIgnoreCase("rev")) {
				if ((card2.getAction().equalsIgnoreCase("plus2") || card2.getAction().equalsIgnoreCase("skip"))
						&& card2.getColor() == this.getColor())
					return true;
				if (card2.getAction().equalsIgnoreCase("rev"))
					return true;
				if ((card2.getAction().equalsIgnoreCase("plus4") || card2.getAction().equalsIgnoreCase("multi"))
						&& card2.getChoosedColor() == this.getColor())
					return true;

				return false;
			}

			// compare skip card with other action card
			if (this.getAction().equalsIgnoreCase("skip")) {
				if ((card2.getAction().equalsIgnoreCase("rev") || card2.getAction().equalsIgnoreCase("plus2"))
						&& card2.getColor() == this.getColor())
					return true;
				if (card2.getAction().equalsIgnoreCase("skip"))
					return true;
				if ((card2.getAction().equalsIgnoreCase("plus4") || card2.getAction().equalsIgnoreCase("multi"))
						&& card2.getChoosedColor() == this.getColor())
					return true;
				return false;
			}
			// compare +2 with other action card
			if (this.getAction().equalsIgnoreCase("plus2")) {
				if ((card2.getAction().equalsIgnoreCase("rev") || card2.getAction().equalsIgnoreCase("skip"))
						&& card2.getColor() == this.getColor())
					return true;
				if (card2.getAction().equalsIgnoreCase("plus2"))
					return true;
				if ((card2.getAction().equalsIgnoreCase("plus4") || card2.getAction().equalsIgnoreCase("multi"))
						&& card2.getChoosedColor() == this.getColor())
					return true;
				return false;
			}
			// compare +4 with other action card
			if (this.getAction().equalsIgnoreCase("plus4")) {
				if (card2.getAction().equalsIgnoreCase("rev") || card2.getAction().equalsIgnoreCase("plus2")
						|| card2.getAction().equalsIgnoreCase("skip") || card2.getAction().equalsIgnoreCase("multi")
						|| card2.getAction().equalsIgnoreCase("plus4"))
					return true;

				else
					return false;
			}

			// compare multi with other action card
			if (this.getAction().equalsIgnoreCase("multi")) {
				if (card2.getAction().equalsIgnoreCase("rev") || card2.getAction().equalsIgnoreCase("plus2")
						|| card2.getAction().equalsIgnoreCase("skip") || card2.getAction().equalsIgnoreCase("multi")
						|| card2.getAction().equalsIgnoreCase("plus4"))
					return true;

				else
					return false;
			}
		}
		return false;
	}

	/**
	 * <b>toString</b>: to display an Action card
	 * 
	 * @return String A graphical display of an action card
	 */
	@Override
	public String toString() {
		String[] card = { " ------- ", "|     |", "|     |", " ------- " };
		String c = "";

		for (int i = 0; i < card.length; i++) {

			for (int j = 0; j < 1; j++) {

				if (i == 1) {

					c = c + "|   " + this.getColor() + "   |" + " ";

				} else if (i == 2) {
					if (this.getAction().equalsIgnoreCase("rev")) {

						c = c + "|  " + this.getAction() + "  |" + " ";
					}

					if (this.getAction().equalsIgnoreCase("skip")) {

						c = c + "| " + this.getAction() + "  |" + " ";
					}
					if (this.getAction().equalsIgnoreCase("multi")) {

						c = c + "| " + this.getAction() + " |" + " ";
					}
					if (this.getAction().equalsIgnoreCase("plus2")) {

						c = c + "|" + "  " + "+2" + "   |" + " ";
					}
					if (this.getAction().equalsIgnoreCase("plus4")) {

						c = c + "|  " + "+4" + "   |" + " ";
					}
				} else {
					c = c + card[i] + " ";
				}

			}
			c += "\n";
		}
		return c;
	}

	/**
	 * <b>compareTo</b> used to compare cards and change its order
	 * 
	 * @param o
	 *            take as parameter other card
	 * @return the return value could be 0, -1 or 1
	 */
	@Override
	public int compareTo(Card o) {
		if (o.getValue() == getValue())
			return 0;
		else if (o.getValue() > getValue())
			return -1;
		else
			return 1;
	}
}
