package at.campus02.unogame;

public abstract class Card implements Comparable<Card> {

	private char color;
	private int value;

	/**
	 * Instanciates a Card
	 * 
	 * @param color:
	 *            R, B, Y or G
	 * @param value:
	 *            the value from 0 to 9
	 * 
	 */
	public Card(char color, int value) {
		this.color = color;
		this.value = value;

	}

	/**
	 * <b>getValue</b> returns the action of a card
	 * 
	 * @return String: from 0 to 9
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <b>getColor</b> returns the color of a card
	 * 
	 * @return char: r, g, b or y
	 */
	public char getColor() {
		return color;
	}

	/**
	 * abstract method that will be later implemented and it depends on the type
	 * cards
	 * 
	 * @param card
	 *            takes a card as parameter
	 * @return true or false
	 */
	public abstract boolean match(Card card);

	/**
	 * abstract method that will be later implemented and it depends on the type
	 * cards It used to display a card
	 * 
	 * @return graphical display of a card
	 */
	public abstract String toString();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + color;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (color != other.color)
			return false;
		if (value != other.value)
			return false;
		return true;
	}

}
