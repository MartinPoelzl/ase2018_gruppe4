package at.campus02.database.unogame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class DB_Client_Uno {

	private Connection connection = null;

	public DB_Client_Uno(String dbName) throws SQLException {
		connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
	}

	/**
	 * @param tableName
	 *            the name of the table to check if the table already exists
	 * @return true or false
	 * @throws SQLException
	 *             if SQL query can not be executed
	 */
	public boolean tableExists(String tableName) throws SQLException {
		String query = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName + "';";
		return executeQuery(query).size() > 0;
	}

	/**
	 * @param sqlStatement
	 *            is the SQL statement that has to be executed
	 * @throws SQLException
	 *             if SQL query can not be executed
	 */
	public void executeStatement(String sqlStatement) throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
		statement.executeUpdate(sqlStatement);
	}

	/**
	 * <b>executeQuery:</b> to execute an SQL query
	 * 
	 * @param sqlQuery
	 *            takes as parameter an SQL query as a string
	 * @return returns the result as ArrayList
	 * @throws SQLException
	 *             if problems with SQL
	 */
	public ArrayList<HashMap<String, String>> executeQuery(String sqlQuery) throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
		ResultSet rs = statement.executeQuery(sqlQuery);
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		while (rs.next()) {
			HashMap<String, String> map = new HashMap<String, String>();
			for (int i = 1; i <= columns; i++) {
				String value = rs.getString(i);
				String key = rsmd.getColumnName(i);
				map.put(key, value);
			}
			result.add(map);
		}
		return result;
	}
}
