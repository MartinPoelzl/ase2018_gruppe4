package at.campus02.unogame;

public class NumberCard extends Card {

	/**
	 * <b>NumberCard</b>: to instance a Number Card
	 * 
	 * @param color
	 *            parameter taken from the class card
	 * @param value
	 *            parameter taken from the class card
	 */
	public NumberCard(char color, int value) {
		super(color, value);

	}

	/**
	 * <b>match</b>: to compare a number card with either Action card or Numbercard
	 * 
	 * @param card
	 *            to check if the card can be played or not
	 * @return true or false
	 *         <ul>
	 *         <li>true: if the number card matches with card</li>
	 *         <li>false: if the number card doesn't match with card</li>
	 *         </ul>
	 */
	@Override
	public boolean match(Card card) {
		if (card instanceof NumberCard) {
			NumberCard cardNumber = (NumberCard) card;

			if (this.getColor() == cardNumber.getColor() || this.getValue() == cardNumber.getValue())
				return true;
		} else {
			ActionCard cardAction = (ActionCard) card;
			if (cardAction.getAction().equalsIgnoreCase("skip") || cardAction.getAction().equalsIgnoreCase("rev")
					|| cardAction.getAction().equalsIgnoreCase("plus2")) {
				if (this.getColor() == cardAction.getColor())
					return true;
			} else {
				if (this.getColor() == cardAction.getChoosedColor())
					return true;
			}

		}
		return false;
	}

	/**
	 * <b>toString</b>:to display an Action card
	 * 
	 * @return String
	 */
	@Override
	public String toString() {

		String[] card = { " ------- ", "|     |", "|     |", " ------- " };
		String c = "";

		for (int i = 0; i < card.length; i++) {

			for (int j = 0; j < 1; j++) {

				if (i == 1) {

					c = c + "|   " + this.getColor() + "   |" + " ";

				}

				else if (i == 2) {

					c = c + "|   " + this.getValue() + "   |" + " ";
				}

				else {
					c = c + card[i] + " ";
				}

			}

			c += "\n";

		}
		return c;
	}

	@Override
	public int compareTo(Card o) {
		if (o.getValue() == getValue())
			return 0;
		else if (o.getValue() > getValue())
			return -1;
		else
			return 1;
	}
}
