package at.campus02.unogame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Bot extends Player {

	/**
	 * <b> Bot </b>: is used to instance a bot
	 * 
	 * @param name:
	 *            the name of bot that will be used to save a bot in the data base
	 */
	public Bot(String name) {
		super(name);
	}

	/**
	 * <b> playCard</b>: checks the hand cards if any card will be matching with the
	 * pile's card
	 * 
	 * @return Card : if there is a matching card
	 * @return null : if there is no matching card
	 */
	@Override
	public Card playCard(Card pileCard) {
		ArrayList<Card> botsHandCard = this.getHandCard();
		Collections.sort(botsHandCard);
		for (Card card : botsHandCard) {
			if (card.match(pileCard)) {
				return card;
			}
		}
		return null;
	}

	/**
	 * <b> chooseColor</b> the bot chooses a random color
	 * 
	 * @param choosedActionCard
	 *            the action card will be passed in this method
	 * @return character the first character of random color: r, g, y, b
	 */
	@Override
	public char chooseColor(ActionCard choosedActionCard) throws IOException {
		return getRandomColor(0, 3);
	}

	/**
	 * <b> getRandomColor</b> generates a random color
	 * 
	 * @param min:
	 *            the start number of the generation
	 * @param max:
	 *            the max number of the generation
	 * @return: the first character of the color
	 *          <ul>
	 *          <li>0: R</li>
	 *          <li>1: G</li>
	 *          <li>2: B</li>
	 *          <li>3: Y</li>
	 *          </ul>
	 */
	private char getRandomColor(int min, int max) {
		char color = ' ';
		Random r = new Random();
		int index = r.nextInt((max - min) + 1) + min;
		switch (index) {
		case 0:
			color = 'R';
			break;
		case 1:
			color = 'G';
			break;
		case 2:
			color = 'B';
			break;
		default:
			color = 'Y';
			break;
		}
		return color;
	}

	/**
	 * <b> sayUno </b> when the bot has 2 cards he returns true
	 * 
	 * @return boolean
	 *         <ul>
	 *         <li>true: if he has 2 cards</li>
	 *         <li>false : if he has more or less cards
	 *         </ul>
	 */
	public boolean sayUno() {
		if (getSizeHandCard() == 2) {
			return true;
		}
		return false;
	}

	@Override
	public void showCards() {
		String[] card = { " ----- ", "|     |", "|     |", " ----- " };
		String c = "";

		for (int i = 0; i < card.length; i++) {
			for (int j = 0; j < super.getSizeHandCard(); j++) {
				c = c + card[i] + " ";
			}
			c += "\n";
		}
		System.out.print(c);
	}

	
	
}
