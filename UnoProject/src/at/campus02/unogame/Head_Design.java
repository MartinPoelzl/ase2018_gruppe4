package at.campus02.unogame;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

public class Head_Design {
	/**
	 * <b>Head_Design</b> is used to create a header with drawing string with stars
	 * "UNO" these method uses the class Graphics2D and BufferedImage
	 */
	public Head_Design() {

		int width = 100;
		int height = 30;

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();
		g.setFont(new Font("SansSerif", Font.BOLD, 14));
		Graphics2D graphics = (Graphics2D) g;
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics.drawString("UNO", 20, 30);

		for (int y = 0; y < height; y++) {
			StringBuilder sb = new StringBuilder();
			for (int x = 0; x < width; x++) {
				sb.append(image.getRGB(x, y) == -16777216 ? " " : "*");
			}

			if (sb.toString().trim().isEmpty()) {
				continue;
			}
			System.out.println(sb);
		}
		System.out.println("              -----------------------------------------------");
		System.out.println("                   The Game starts only with 4 players");
		System.out.println("              -----------------------------------------------");
	}
}
