package at.campus02.test.unogame;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;

import at.campus02.unogame.ActionCard;
import at.campus02.unogame.Card;
import at.campus02.unogame.NumberCard;
import at.campus02.unogame.Player;

/**
 * PLayerTest: tests the different methods: addHandCard, getHandCard,
 * sumValuesHandCards, getNextPLayer
 */
public class PlayerTest {

	Player player1 = new Player("maroua");
	NumberCard c1 = new NumberCard('R', 5);
	NumberCard c2 = new NumberCard('Y', 7);

	ActionCard ac1 = new ActionCard('Y', 20, "skip");
	ActionCard ac2 = new ActionCard('M', 50, "multi");

	@Test
	public void addHandCard() {
		player1.addHandCard(ac1);
		player1.addHandCard(c2);
		player1.addHandCard(ac2);
		player1.addHandCard(c1);

		ArrayList<Card> expectedArray = new ArrayList<>();
		expectedArray.add(ac1);
		expectedArray.add(c2);
		expectedArray.add(ac2);
		expectedArray.add(c1);

		assertEquals(player1.getHandCard().size(), 4);
		assertEquals(player1.getHandCard(), expectedArray);

	}

	/**
	 * <b>sumValuesHandCards:</b> to test the sumValuesHandCards function
	 */
	@Test
	public void sumValuesHandCards() {
		player1.addHandCard(ac1);
		player1.addHandCard(c2);
		player1.addHandCard(ac2);
		player1.addHandCard(c1);

		int expectedsum = 82;

		int sum = player1.sumValuesHandCards();

		assertEquals(sum, expectedsum);
	}

	/**
	 * <b>getNextPlayer:</b> test if the turn of player is right
	 */
	@Test
	public void getNextPlayer() {
		Player nextPlayer = new Player("eva");
		player1.setNextPlayer(nextPlayer);

		String nameNextPlayer = player1.getNextPlayer().getName();
		String expectedResult = nextPlayer.getName();

		assertEquals(nameNextPlayer, expectedResult);

	}

}
